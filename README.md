# GlobalLogic API-REST

Este proyecto contempla el desarrollo de una **API-REST** mediante el uso de tecnologías **Spring** para la manejo de Usuarios
a traves de la implementación de un CRUD.

### Tecnologías Utilizadas
    - Jdk8
    - Spring Boot
    - Spring Security
    - Spring Data JPA
    - H2 Database
    - Git
    - Gradle
    - Spock Framework
    - Postman
    - Groovy
    - OAuth2
    
    
### Instrucciones de Instalación

Una vez descargado el proyecto, se deben seguir los siguientes pasos:

Nos ubicamos en la ruta de la raíz del proyecto "globallogic-api-rest" y abrimos una terminal para ejecutar el
siguiente comando

    - gradle build
    
Con esto generamos los compilados del código fuente.

Una vez que se han generado los compilados, para desplegar el proyecto ejecutamos el siguiente comando

    - gradle run
    
Al ejecutar este comando estamos efectuando las siguientes acciones:
 
- Levantamos el proyecto de Spring Boot.
- Levantamos la base de datos en memoria H2.
- Levantamos la consola ó interfaz gráfica de la base de datos en memoria H2.
- Creamos desde cero todas las tablas de base de datos necesarias para la aplicación.
- Ejecutamos el script "data.sql" con el cual poblamos con datos de prueba las tablas de base de datos.
- Tenemos la API-REST preparada para su uso.

### URLs de la API-REST
Las siguientes son las URL con las cuales podemos hacer uso de la API-REST

    - Generación Token:             http://localhost:8080/oauth/token
    - Consultar todos los Usuarios: http://localhost:8080/api/user/get-all-users
    - Consultar Usuario por ID:     http://localhost:8080/api/user/get-user/{id}
    - Creación de Usuario:          http://localhost:8080/api/user/create-user
    - Actualizar Usuario:           http://localhost:8080/api/user/update-user/{id}
    - Borrar Usuario:               http://localhost:8080/api/user/delete-user/{id}

### Base de Datos H2

Si deseamos explorar la base de datos en un entorno mas tradicional, podemos acceder a la consola de H2
en la siguiente dirección

    - http://localhost:8080/h2-console/

En la pantalla inicial ingresamos los siguientes campos

    - Saved Settings: Generic H2 (Embedded)
    - Settin Name:    Generic H2 (Embedded)
    - Driver Class:   org.h2.Driver
    - JDBC URL:       jdbc:h2:mem:testdb
    - User Name:      admin
    - Password:       admin
    
Finalmente podemos hacer click en la opción **"Test Connection"**, ó directamente en **"Connect"** y podremos explorar 
la base de datos.
 
### Ejecutar Pruebas Unitarias

Para ejecutar las pruebas unitarias escritas con **"Spock Framework"** debemos estar ubicados en la raíz del proyecto,
abrir una terminal y ejecutar el siguiente comando

    - gradle clean test
    
Con esto se ejecutarán automáticamente todas las pruebas contempladas, y además, se generá un reporte de las pruebas
en un archivo **HTML** en la siguiente ubicación

    -  /globallogic-api-rest/build/reports/index.html
    
### Ejecutar Pruebas Manuales API-REST

Tenemos la opción de ejecutar las pruebas manuales y probar las API-REST mediante el uso de **"Postman"**.
Para ello debemos tener la API-REST desplegada y listo para su uso, luego debemos iniciar la aplicación **Postman**
y en ella vamos a importar un archivo de colección llamado **"API-REST-GlobalLogic.postman_collection.json"** que se
encuentra en la siguiente ubicación

    - /globallogic-api-rest/postman/API-REST-GlobalLogic.postman_collection.json
    
Para importarlo debemos estar en Postman y en la esquina superior izquierda hacemos click en la opción "Import"}
e importamos el archivo, con esto tendremos la API lista para ser consumida.

Luego para poder ejecutar las operaciones CRUD tenemos que generar un token previamente, para ello seguiremos los
siguientes pasos:

- Abrimos la API para Generación de Token

En esta pestaña nos ubicamos en la sección **"Authorization"** y en Type seleccionamos **"Basic Auth"**, luego 
ingresamos las siguientes credenciales

    - Username: frontend
    - Password: 12345
    
- Nos dirigimos a la sección **"Body"**

Seleccionamos **"x-www-form-urlencoded"** y agregamos las siguientes variables

    - username:   admin
    - password:   Abcde00
    - grant_type: password
    
- Hacemos click en **"Send"** y se nos genera el token en el campo **"access_token"** del JSON obtenido.

- Este token lo copiamos y pegamos en el campo **"Token"** de cualquiera de las otras operaciones HTTP. Este campos se
encuentra en la sección **"Authorization"**, en Type seleccionamos **"Bearer Token"**, y finalmente pegamos el token y
ahora si podremos hacer pruebas sobre nuestra API.

### Trazas LOG

En la siguiente ubicación podemos encontrar el archivo log generado por la ejecución de la aplicación

    - /globallogic-api-rest/logs/globallogic-api-rest.log
    
### Diagramas UML

En la siguiente ubicación podemos encontrar el Diagrama de Componentes y Diagrama de Secuencia del proyecto

    - /globallogic-api-rest/diagramas/