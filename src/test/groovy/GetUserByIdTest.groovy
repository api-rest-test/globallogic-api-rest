import com.globallogic.globallogicapirest.controller.UserController
import com.globallogic.globallogicapirest.entity.User
import com.globallogic.globallogicapirest.service.UserService
import mock.UserMock
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class GetUserByIdTest extends Specification {

    UserService userService
    UserController userController
    UserMock userMock = new UserMock()

    def setup() {
        userService = Mock(UserService)
        userController = new UserController(userService)
    }

    def 'HTTP 200 - GET - Consultar un usuario dado un id'() {
        given: 'una lista de usuarios'
        userService.getUserById(1L) >> userMock.getUsers().get(0)

        when: 'llamo a la api para obtener usuario por id'
        ResponseEntity<User> response = userController.getUser(1L)

        then: 'obtengo un usuario dado un id'
        assert response.every {it.getStatusCodeValue() == 200 && it.getBody() != null}
    }

    def 'HTTP 404 - GET - Consultar un usuario dado un id que no existe'() {
        given: 'una lista de usuarios'
        userService.getUserById(1L) >> userMock.getUsers().get(0)

        when: 'llamo a la api para obtener usuario por id'
        ResponseEntity<User> response = userController.getUser(1000L)

        then: 'No puedo obtener el usuario'
        assert response.every {it.getStatusCodeValue() == 404 && it.getBody() != null}
    }

    def 'HTTP 500 - GET - Consultar un usuario genera una excepción y mensaje de error'() {
        given: 'un componente falla dentro de la ejecución'
        userService.getUserById(1L) >> -1

        when: 'llamo a la api para obtener todos los usuarios'
        ResponseEntity<List<User>> response = userController.getUser(1L)

        then: 'capturo la excepción y retorno un mensaje de error'
        assert response.every {it.getStatusCodeValue() == 500 && it.getBody() != null}
    }
}
