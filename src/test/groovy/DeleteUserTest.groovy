import com.globallogic.globallogicapirest.controller.UserController
import com.globallogic.globallogicapirest.entity.User
import com.globallogic.globallogicapirest.service.UserService
import mock.UserMock
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import spock.lang.Specification

class DeleteUserTest extends Specification {

    UserService userService
    UserController userController
    UserMock userMock = new UserMock()
    BindingResult bindingResult
    String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZS" +
            "JdLCJuYW1lIjoiQWRtaW5zaXRyYWRvciIsImV4cCI6MTYwNjc2NjEzNywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVf" +
            "VVNFUiJdLCJqdGkiOiJkMDIwODk4My1iMmU1LTQ4N2MtOWQzOS1lMzJjOGE4NDk4MmUiLCJlbWFpbCI6ImFkbWluQGN1YWxxdWllcm" +
            "EuY29tIiwiY2xpZW50X2lkIjoiZnJvbnRlbmQifQ.muqgAjsRiLr1Rua5ISk750rD1pLCElFdMD1rRTs8X8g"


    def setup() {
        bindingResult = Mock(BindingResult)
        userService = Mock(UserService)
        userController = new UserController(userService)
    }

    def 'HTTP 200 - DELETE - Usuario eliminado correctamente'() {
        given: 'el id de un usuario a eliminar'
        userService.deleteUser(1L)

        when: 'llamo a la api para eliminación de usuario'
        ResponseEntity<List<User>> response = userController.deleteUser(1L)

        then: 'obtengo una respuesta correcta y los datos de transacción'
        assert response.every {it.getStatusCodeValue() == 200 && it.getBody() != null}
    }

    def 'HTTP 500 - DELETE - La eliminación del usuario generó una excepción'() {
        given: 'el id de un usuario que será eliminado del sistema'
        User user = userMock.getUsers().get(0)
        userService.deleteUser(1L) >> user

        when: 'llamo a la api para eliminación de usuario'
        ResponseEntity<List<User>> response = userController.deleteUser(1L)

        then: 'capturo la excepción y retorno un mensaje de error'
        assert response.every {it.getStatusCodeValue() == 500 && it.getBody() != null}
    }
}
