import com.globallogic.globallogicapirest.controller.UserController
import com.globallogic.globallogicapirest.entity.User
import com.globallogic.globallogicapirest.service.UserService
import mock.UserMock
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import spock.lang.Specification

class CreateUserTest extends Specification {

    UserService userService
    UserController userController
    UserMock userMock = new UserMock()
    BindingResult bindingResult
    String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZS" +
            "JdLCJuYW1lIjoiQWRtaW5zaXRyYWRvciIsImV4cCI6MTYwNjc2NjEzNywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVf" +
            "VVNFUiJdLCJqdGkiOiJkMDIwODk4My1iMmU1LTQ4N2MtOWQzOS1lMzJjOGE4NDk4MmUiLCJlbWFpbCI6ImFkbWluQGN1YWxxdWllcm" +
            "EuY29tIiwiY2xpZW50X2lkIjoiZnJvbnRlbmQifQ.muqgAjsRiLr1Rua5ISk750rD1pLCElFdMD1rRTs8X8g"


    def setup() {
        bindingResult = Mock(BindingResult)
        userService = Mock(UserService)
        userController = new UserController(userService)
    }

    def 'HTTP 201 - POST - Creación de un nuevo usuario'() {
        given: 'un usuario a persistir'
        User user = userMock.getUsers().get(0)
        userService.createUser(user) >> user

        when: 'llamo a la api para creación de usuario'
        ResponseEntity<List<User>> response = userController.createUser(user, bindingResult, token)

        then: 'obtengo una respuesta correcta y los datos de transacción'
        assert response.every {it.getStatusCodeValue() == 201 && it.getBody() != null}
    }

    def 'HTTP 200 - POST - Verificación de existencia de correo al crear nuevo usuario'() {
        given: 'un usuario a persistir'
        User user = userMock.getUsers().get(0)
        userService.createUser(user) >> user
        userService.getUserByEmail(user.getEmail()) >> user

        when: 'llamo a la api para creación de usuario y con un usuario que tiene un correo existente'
        ResponseEntity<List<User>> response = userController.createUser(user, bindingResult, token)

        then: 'obtengo un mensaje que nos indica El correo ya registrado'
        assert response.every {it.getStatusCodeValue() == 200 && it.getBody() != null}
    }

    def 'HTTP 400 - POST - Creación de un nuevo usuario contiene errores en BindingResult'() {
        given: 'un usuario a persistir con una lista de errores en BindingResult'
        User user = userMock.getUsers().get(0)
        userService.createUser(user) >> user
        userService.getUserByEmail(user.getEmail()) >> "error"
        FieldError fieldError = new FieldError("email", "email", "Formato de correo no valido")
        bindingResult.hasErrors() >> fieldError
        bindingResult.getFieldErrors() >> [fieldError]

        when: 'llamo a la api para creación de usuario'
        ResponseEntity<List<User>> response = userController.createUser(user, bindingResult, token)

        then: 'capturo la excepción y retorno un mensaje de error'
        assert response.every {it.getStatusCodeValue() == 400 && it.getBody() != null}
    }
}