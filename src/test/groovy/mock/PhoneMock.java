package mock;

import com.globallogic.globallogicapirest.entity.Phone;
import java.util.ArrayList;
import java.util.List;

public class PhoneMock {
    Phone phone1 = new Phone();
    Phone phone2 = new Phone();
    Phone phone3 = new Phone();
    Phone phone4 = new Phone();
    List<Phone> phones = new ArrayList<>();

    public PhoneMock() {
        phone1.setId(1L);
        phone1.setNumber("111111111");
        phone1.setCityCode("1");
        phone1.setCountryCode("2");
        phones.add(phone1);

        phone2.setId(2L);
        phone2.setNumber("222222222");
        phone2.setCityCode("1");
        phone2.setCountryCode("2");
        phones.add(phone2);

        phone3.setId(3L);
        phone3.setNumber("333333333");
        phone3.setCityCode("1");
        phone3.setCountryCode("2");
        phones.add(phone3);

        phone4.setId(4L);
        phone4.setNumber("444444444");
        phone4.setCityCode("1");
        phone4.setCountryCode("2");
        phones.add(phone4);

        setPhones(phones);
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @Override
    public String toString() {
        return "PhoneMock{" +
                "phone1=" + phone1 +
                ", phone2=" + phone2 +
                ", phone3=" + phone3 +
                ", phone4=" + phone4 +
                ", phones=" + phones +
                '}';
    }
}
