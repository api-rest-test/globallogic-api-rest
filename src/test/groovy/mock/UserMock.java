package mock;

import com.globallogic.globallogicapirest.entity.User;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

public class UserMock {

    User user1 = new User();
    User user2 = new User();
    User admin = new User();
    List<User> users = new ArrayList<>();
    PhoneMock phoneMock = new PhoneMock();
    RoleMock roleMock = new RoleMock();

    public UserMock() {
        user1.setId(1L);
        user1.setName("Andres Oropeza");
        user1.setUserName("andres");
        user1.setPassword("$2a$10$9DFKm7zBA0hfzi7bFlcQAOAtmHl7PXs1TbyF4GZr.qfi1ciSthVTa");
        user1.setEmail("andres.oropeza@cualquiera.com");
        user1.setCreated(Calendar.getInstance());
        user1.setModified(Calendar.getInstance());
        user1.setLast_login(Calendar.getInstance());
        user1.setPhones(phoneMock.getPhones().stream().filter(phone -> phone.getId() == 1L || phone.getId() == 2L)
                .collect(Collectors.toList()));
         user1.setRoles(roleMock.getRoles().stream().filter(role -> role.getId() == 1L).collect(Collectors.toList()));
        user1.setActive(true);
        users.add(user1);

        user2.setId(2L);
        user2.setName("Juan Hernandez");
        user2.setUserName("juan");
        user2.setPassword("$2a$10$9DFKm7zBA0hfzi7bFlcQAOAtmHl7PXs1TbyF4GZr.qfi1ciSthVTa");
        user2.setEmail("juan.hernandez@cualquiera.com");
        user2.setCreated(Calendar.getInstance());
        user2.setModified(Calendar.getInstance());
        user2.setLast_login(Calendar.getInstance());
        user2.setPhones(phoneMock.getPhones().stream().filter(phone -> phone.getId() == 3L)
                .collect(Collectors.toList()));
        user2.setRoles(roleMock.getRoles().stream().filter(role -> role.getId() == 1L).collect(Collectors.toList()));
        user2.setActive(true);
        users.add(user2);

        admin.setId(3L);
        admin.setName("Administrador");
        admin.setUserName("admin");
        admin.setPassword("$2a$10$9DFKm7zBA0hfzi7bFlcQAOAtmHl7PXs1TbyF4GZr.qfi1ciSthVTa");
        admin.setEmail("admin@cualquiera.com");
        admin.setCreated(Calendar.getInstance());
        admin.setModified(Calendar.getInstance());
        admin.setLast_login(Calendar.getInstance());
        admin.setPhones(phoneMock.getPhones().stream().filter(phone -> phone.getId() == 4L)
                .collect(Collectors.toList()));
        admin.setRoles(roleMock.getRoles().stream().filter(role -> role.getId() == 1L && role.getId() == 1L)
                .collect(Collectors.toList()));
        admin.setActive(true);
        users.add(admin);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<User> getEmptyUsers() {
        List<User> empty = new ArrayList<>();
        return empty;
    }

    @Override
    public String toString() {
        return "UserMock{" +
                "user1=" + user1 +
                ", user2=" + user2 +
                ", admin=" + admin +
                ", users=" + users +
                ", phoneMock=" + phoneMock +
                ", roleMock=" + roleMock +
                '}';
    }
}
