package mock;

import com.globallogic.globallogicapirest.entity.Role;
import java.util.ArrayList;
import java.util.List;

public class RoleMock {
    Role role1  = new Role();
    Role role2  = new Role();
    List<Role> roles = new ArrayList<>();

    public RoleMock() {
        role1.setId(1L);
        role1.setName("ROLE_USER");
        roles.add(role1);

        role2.setId(2L);
        role2.setName("ROLE_ADMIN");
        roles.add(role2);

        setRoles(roles);
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "RoleMock{" +
                "role1=" + role1 +
                ", role2=" + role2 +
                ", roles=" + roles +
                '}';
    }
}
