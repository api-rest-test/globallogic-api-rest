import com.globallogic.globallogicapirest.controller.UserController
import com.globallogic.globallogicapirest.entity.User
import com.globallogic.globallogicapirest.service.UserService
import mock.UserMock
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class GetAllUserTest extends Specification {

    UserService userService
    UserController userController
    UserMock userMock = new UserMock()

    def setup() {
        userService = Mock(UserService)
        userController = new UserController(userService)
    }

    def 'HTTP 200 - GET - Consultar todos los usuarios'() {
        given: 'una lista de usuarios'
        userService.getAllUsers() >> userMock.getUsers()

        when: 'llamo a la api para obtener todos los usuarios'
        ResponseEntity<List<User>> response = userController.getUsers()

        then: 'obtengo una lista de usuarios'
        assert response.every {it.getStatusCodeValue() == 200
                && it.getBody().size() > 0 && it.getBody() != null}
    }

    def 'HTTP 404 - GET - Al consultar todos los usuarios no se obtiene ningun resultado'() {
        given: 'una lista vacía de usuarios'
        userService.getAllUsers() >> userMock.getEmptyUsers()

        when: 'llamo a la api para obtener todos los usuarios'
        ResponseEntity<List<User>> response = userController.getUsers()

        then: 'obtengo una lista de usuarios'
        assert response.every {it.getStatusCodeValue() == 404 && it.getBody() != null}
    }

    def 'HTTP 500 - GET - Consultar todos los usuarios genera una excepción y mensaje de error'() {
        given: 'un componente falla dentro de la ejecución'
        userService.getAllUsers() >> -1

        when: 'llamo a la api para obtener todos los usuarios'
        ResponseEntity<List<User>> response = userController.getUsers()

        then: 'capturo la excepción y retorno un mensaje de error'
        assert response.every {it.getStatusCodeValue() == 500 && it.getBody() != null}
    }
}
