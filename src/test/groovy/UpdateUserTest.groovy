import com.globallogic.globallogicapirest.controller.UserController
import com.globallogic.globallogicapirest.entity.User
import com.globallogic.globallogicapirest.service.UserService
import mock.UserMock
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import spock.lang.Specification

class UpdateUserTest extends Specification {

    UserService userService
    UserController userController
    UserMock userMock = new UserMock()
    BindingResult bindingResult
    String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZS" +
            "JdLCJuYW1lIjoiQWRtaW5zaXRyYWRvciIsImV4cCI6MTYwNjc2NjEzNywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVf" +
            "VVNFUiJdLCJqdGkiOiJkMDIwODk4My1iMmU1LTQ4N2MtOWQzOS1lMzJjOGE4NDk4MmUiLCJlbWFpbCI6ImFkbWluQGN1YWxxdWllcm" +
            "EuY29tIiwiY2xpZW50X2lkIjoiZnJvbnRlbmQifQ.muqgAjsRiLr1Rua5ISk750rD1pLCElFdMD1rRTs8X8g"


    def setup() {
        bindingResult = Mock(BindingResult)
        userService = Mock(UserService)
        userController = new UserController(userService)
    }

    def 'HTTP 200 - PUT - Actualización de un usuario existente'() {
        given: 'un usuario a actualizar'
        User user = userMock.getUsers().get(0)
        userService.getUserById(user.getId()) >> user
        userService.updateUser(user) >> user

        when: 'llamo a la api para actualización de usuario'
        ResponseEntity<List<User>> response = userController.updateUser(user, bindingResult, user.getId(), token)

        then: 'obtengo una respuesta correcta y los datos de transacción'
        assert response.every {it.getStatusCodeValue() == 200 && it.getBody() != null}
    }

    def 'HTTP 200 - PUT - Verificación de existencia de correo al actualizar un usuario existente'() {
        given: 'un usuario a actualizar'
        User user = userMock.getUsers().get(0)
        UserMock userMockEmailExiste = new UserMock()
        User userEmailExiste = userMockEmailExiste.getUsers().get(0)
        userService.getUserById(user.getId()) >> user
        userEmailExiste.setEmail("email.existe@cualquier.com")
        userService.getUserByEmail(userEmailExiste.getEmail()) >> userEmailExiste

        when: 'llamo a la api para actualización de usuario y con un usuario que tiene un correo existente'
        ResponseEntity<List<User>> response = userController.updateUser(userEmailExiste, bindingResult, userEmailExiste.getId(), token)

        then: 'obtengo un mensaje que nos indica El correo ya registrado'
        assert response.every {it.getStatusCodeValue() == 200 && it.getBody() != null}
    }

    def 'HTTP 400 - PUT - Actualización de usuario existente contiene errores en BindingResult'() {
        given: 'un usuario a persistir con una lista de errores en BindingResult'
        User user = userMock.getUsers().get(0)
        userService.updateUser(user) >> user
        userService.getUserByEmail(user.getEmail()) >> "error"
        FieldError fieldError = new FieldError("email", "email", "Formato de correo no valido")
        bindingResult.hasErrors() >> fieldError
        bindingResult.getFieldErrors() >> [fieldError]

        when: 'llamo a la api para actualiación de usuario'
        ResponseEntity<List<User>> response = userController.updateUser(user, bindingResult, user.getId(), token)

        then: 'capturo la excepción y retorno un mensaje de error'
        assert response.every {it.getStatusCodeValue() == 400 && it.getBody() != null}
    }
}