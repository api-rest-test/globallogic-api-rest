package com.globallogic.globallogicapirest.repository;

import com.globallogic.globallogicapirest.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long> {

    /**
     * Consulta JPA que obtiene un usuario dado su username
     * @param username
     * @return Usuario
     */
    public User findByUserName(String username);

    /**
     * Consulta JPA que obtiene un usuario dado su correo
     * @param email
     * @return Usuario
     */
    public User findByEmail(String email);
}
