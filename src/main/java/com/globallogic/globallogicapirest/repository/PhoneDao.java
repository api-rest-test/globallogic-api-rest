package com.globallogic.globallogicapirest.repository;

import com.globallogic.globallogicapirest.entity.Phone;
import org.springframework.data.repository.CrudRepository;

public interface PhoneDao extends CrudRepository<Phone, Long> {
}
