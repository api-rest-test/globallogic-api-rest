package com.globallogic.globallogicapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GloballogicApiRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GloballogicApiRestApplication.class, args);
    }
}
