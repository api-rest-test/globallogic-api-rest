package com.globallogic.globallogicapirest.controller;

import javax.validation.Valid;
import com.globallogic.globallogicapirest.entity.User;
import com.globallogic.globallogicapirest.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Operación GET que obtiene todos los usuarios de base de datos
     * @return Body con respuesta HTTP
     */
    @GetMapping("/get-all-users")
    public ResponseEntity<?> getUsers() {
        logger.info("Inicia metodo: /api/user/get-all-users");
        List<User> users;
        ResponseEntity<?> response;
        Map<String, Object> responseError = new HashMap<>();

        try {
            users = userService.getAllUsers();
            if (users != null && !users.isEmpty()) {
                response = ResponseEntity.ok(users);
            } else {
                logger.error("No existen usuarios registrados");
                responseError.put("mensaje", "No existen usuarios registrados");
                response = new ResponseEntity<>(responseError, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            logger.error("Se produjo un error al intentar consultar todos los usuarios");
            logger.error("Error: " + e.getMessage(), e);
            responseError.put("mensaje", "Se produjo un error al intentar consultar todos los usuarios");
            response = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("Finaliza metodo: /api/user/get-all-users");
        return response;
    }

    /**
     * Operación GET que obtiene un usuario de base de datos dado su id
     * @param id
     * @return Body con respuesta HTTP
     */
    @GetMapping("/get-user/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        logger.info("Inicia metodo: /api/user/get-user/{id}");
        User user;
        ResponseEntity<?> response;
        Map<String, Object> responseError = new HashMap<>();

        try {
            user = userService.getUserById(id);
            if (user != null) {
                response = ResponseEntity.ok(user);
            } else {
                logger.error("No existe un usuario con el id: ".concat(id.toString()));
                responseError.put("mensaje", "No existe un usuario con el id: ".concat(id.toString()));
                response = new ResponseEntity<>(responseError, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            logger.error("Se produjo un error al consultar al usuario con el id: ".concat(id.toString()));
            logger.error("Error: " + e.getMessage(), e);
            responseError.put("mensaje", "Se produjo un error al consultar al usuario con el id: "
                    .concat(id.toString()));
            response = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("Finaliza metodo: /api/user/get-user/{id}");
        return response;
    }

    /**
     * Operación POST para crear un nuevo usuario
     * @param user
     * @param result
     * @param token
     * @return Body con respuesta HTTP
     */
    @PostMapping("/create-user")
    public ResponseEntity<?> createUser(@Valid @RequestBody User user, BindingResult result,
                                           @RequestHeader(name="Authorization") String token) {
        logger.info("Inicia metodo: /api/user/create-user");
        ResponseEntity<?> responseEntity;
        Map<String, Object> responseError;
        try {
            if(result.hasErrors()) {
                responseError = new HashMap<>();
                List<String> errors = result.getFieldErrors()
                        .stream()
                        .map(error -> "Campo: " + error.getField() +" => Mensaje: "+ error.getDefaultMessage())
                        .collect(Collectors.toList());

                responseError.put("mensaje", errors);
                responseEntity = new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                if (userService.getUserByEmail(user.getEmail()) != null) {
                    logger.error("El correo ya registrado");
                    responseError = new HashMap<>();
                    responseError.put("mensaje", "El correo ya registrado");
                    responseEntity = new ResponseEntity<>(responseError, HttpStatus.OK);
                } else {
                    user.setCreated(Calendar.getInstance());
                    user.setModified(Calendar.getInstance());
                    user.setLast_login(Calendar.getInstance());
                    user.setActive(true);
                    User createdUser = userService.createUser(user);

                    Map<String, Object> response = new HashMap<>();
                    response.put("id",createdUser.getId());
                    response.put("created",createdUser.getCreated());
                    response.put("modified",createdUser.getModified());
                    response.put("last_login",createdUser.getLast_login());
                    response.put("token",token);
                    response.put("isactive",createdUser.getActive());
                    responseEntity = new ResponseEntity<>(response, HttpStatus.CREATED);
                }
            }
        } catch (Exception e) {
            logger.error("Se produjo un error al crear el usuario");
            logger.error("Error: " + e.getMessage(), e);
            responseError = new HashMap<>();
            responseError.put("mensaje", "Se produjo un error al crear el usuario");
            responseEntity = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("Finaliza metodo: /api/user/create-user");
        return responseEntity;
    }

    /**
     * Operación POST para actualizar un usuario
     * @param user
     * @param result
     * @param token
     * @return Body con respuesta HTTP
     */
    @PutMapping("/update-user/{id}")
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user, BindingResult result,
                                           @PathVariable("id") Long id,
                                           @RequestHeader(name="Authorization") String token) {
        logger.info("Inicia metodo: /api/user/update-user/{id}");
        ResponseEntity<?> responseEntity;
        Map<String, Object> responseError;
        User currentUser;
        User userEmail;
        try {
            if(result.hasErrors()) {
                responseError = new HashMap<>();
                List<String> errors = result.getFieldErrors()
                        .stream()
                        .map(error -> "Campo: " + error.getField() +" => Mensaje: "+ error.getDefaultMessage())
                        .collect(Collectors.toList());

                responseError.put("mensaje", errors);
                responseEntity = new ResponseEntity<>(responseError, HttpStatus.BAD_REQUEST);
            } else {
                currentUser  = userService.getUserById(id);
                if (currentUser != null) {

                    userEmail = userService.getUserByEmail(user.getEmail());

                    if (userEmail != null && !currentUser.getEmail().equalsIgnoreCase(user.getEmail())) {
                        logger.error("Este correo ya se encuentra registrado: ".concat(user.getEmail()));
                        responseError = new HashMap<>();
                        responseError.put("mensaje", "Este correo ya se encuentra registrado: "
                                .concat(user.getEmail()));
                        responseEntity = new ResponseEntity<>(responseError, HttpStatus.OK);
                    } else {
                        user.setCreated(currentUser.getCreated());
                        user.setModified(Calendar.getInstance());
                        user.setLast_login(Calendar.getInstance());
                        user.setActive(true);
                        User updatedUser = userService.updateUser(user);

                        Map<String, Object> response = new HashMap<>();
                        response.put("id",updatedUser.getId());
                        response.put("created",updatedUser.getCreated());
                        response.put("modified",updatedUser.getModified());
                        response.put("last_login",updatedUser.getLast_login());
                        response.put("token",token);
                        response.put("isactive",updatedUser.getActive());
                        responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
                    }
                } else {
                    String messageError = "No se puede actualizar usuario con el id ".concat(id.toString())
                            .concat(", no se encuentra registrado");
                    logger.error(messageError);
                    responseError = new HashMap<>();
                    responseError.put("mensaje", messageError);
                    responseEntity = new ResponseEntity<>(responseError, HttpStatus.NOT_FOUND);
                }
            }

        } catch (Exception e) {
            logger.error("Se produjo un error al actualizar el usuario");
            logger.error("Error: " + e.getMessage(), e);
            responseError = new HashMap<>();
            responseError.put("mensaje", "Se produjo un error al actualizar el usuario");
            responseEntity = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("Finaliza metodo: /api/user/update-user/{id}");
        return responseEntity;
    }

    /**
     * Operación DELETE para eliminar un usuario dado un id
     * @param id)
     * @return Body con respuesta HTTP
     */
    @DeleteMapping("/delete-user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        logger.info("Inicia metodo: /api/user/delete-user/{id}");
        ResponseEntity<?> responseEntity;
        Map<String, Object> responseError;
        try {
            userService.deleteUser(id);
            Map<String, Object> response = new HashMap<>();
            response.put("mensaje", "El usuario con el id ".concat(id.toString())
                    .concat(" fue eliminado correctamente"));
            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Se produjo un error al eliminar el usuario con el id: ".concat(id.toString()));
            logger.error("Error: " + e.getMessage(), e);
            responseError = new HashMap<>();
            responseError.put("mensaje", "Se produjo un error al eliminar el usuario con el id: "
                    .concat(id.toString()));
            responseEntity = new ResponseEntity<>(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("Finaliza metodo: /api/user/delete-user/{id}");
        return responseEntity;
    }
}
