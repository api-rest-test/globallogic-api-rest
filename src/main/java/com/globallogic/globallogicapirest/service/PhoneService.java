package com.globallogic.globallogicapirest.service;

import com.globallogic.globallogicapirest.entity.Phone;

import java.util.List;

public interface PhoneService {

    /**
     * Consulta JPA que obtiene todos los telefonos
     * @return Lista de telefonos
     */
    public List<Phone> getAllPhones();

    /**
     * Consulta JPA que obtiene un telefono dado su id
     * @param id
     * @return Telefono
     */
    public Phone getPhoneById(Long id);

    /**
     * Consulta JPA que crea un nuevo telefono
     * @param phone
     * @return Telefono
     */
    public Phone createPhone(Phone phone);

    /**
     * Consulta JPA que actualiza un telefono
     * @param phone
     * @return Telefono
     */
    public Phone updatePhone(Phone phone);

    /**
     * Consulta JPA que elimnina un telefono dado un id
     * @param id
     */
    public void deletePhone(Long id);
}
