package com.globallogic.globallogicapirest.service;

import com.globallogic.globallogicapirest.entity.User;
import com.globallogic.globallogicapirest.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    /**
     * Consulta JPA que obtiene todos los usuarios
     * @return Lista de usuarios
     */
    @Override
    public List<User> getAllUsers() {
        return (List<User>) userDao.findAll();
    }

    /**
     * Consulta JPA que obtiene un usuario dado un id
     * @param id
     * @return Usuario
     */
    @Override
    public User getUserById(Long id) {
        Optional<User> optional = userDao.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    /**
     * Consulta JPA que crea un nuevo usuario
     * @param user
     * @return Usuario
     */
    @Override
    public User createUser(User user) {
        return userDao.save(user);
    }

    /**
     * Consulta JPA que actualiza un usuario
     * @param user
     * @return Usuario
     */
    @Override
    public User updateUser(User user) {
        return userDao.save(user);
    }

    /**
     * Consulta JPA que elimina un usuario dado un id
     * @param id
     */
    @Override
    public void deleteUser(Long id) {
        userDao.deleteById(id);
    }

    /**
     * Consulta JPA que obtiene un usuario dado un username
     * @param username
     * @return Usuario
     */
    @Override
    public User loadUserByUsername(String username) {
        return userDao.findByUserName(username);
    }

    /**
     * Consulta JPA que obtiene un usuario dado un email
     * @param email
     * @return Usuario
     */
    @Override
    public User getUserByEmail(String email) {
        return userDao.findByEmail(email);
    }
}
