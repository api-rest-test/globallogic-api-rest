package com.globallogic.globallogicapirest.service;

import com.globallogic.globallogicapirest.entity.Phone;
import com.globallogic.globallogicapirest.repository.PhoneDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PhoneServiceImpl implements com.globallogic.globallogicapirest.service.PhoneService {

    @Autowired
    PhoneDao phoneDao;

    /**
     * Consulta JPA que obtiene todos los telefonos
     * @return Lista de telefonos
     */
    @Override
    public List<Phone> getAllPhones() {
        return (List<Phone>) phoneDao.findAll();
    }

    /**
     * Consulta JPA que obtiene un telefono dado su id
     * @param id
     * @return Telefono
     */
    @Override
    public Phone getPhoneById(Long id) {
        return phoneDao.findById(id).get();
    }

    /**
     * Consulta JPA que crea un nuevo telefono
     * @param phone
     * @return Telefono
     */
    @Override
    public Phone createPhone(Phone phone) {
        return phoneDao.save(phone);
    }

    /**
     * Consulta JPA que actualiza un telefono
     * @param phone
     * @return Telefono
     */
    @Override
    public Phone updatePhone(Phone phone) {
        return phoneDao.save(phone);
    }

    /**
     * Consulta JPA que elimnina un telefono dado un id
     * @param id
     */
    @Override
    public void deletePhone(Long id) {
        phoneDao.deleteById(id);
    }
}
