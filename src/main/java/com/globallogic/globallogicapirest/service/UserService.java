package com.globallogic.globallogicapirest.service;

import com.globallogic.globallogicapirest.entity.User;
import java.util.List;

public interface UserService {

    /**
     * Consulta JPA que obtiene todos los usuarios
     * @return Lista de usuarios
     */
    public List<User> getAllUsers();

    /**
     * Consulta JPA que obtiene un usuario dado un id
     * @param id
     * @return Usuario
     */
    public User getUserById(Long id);

    /**
     * Consulta JPA que crea un nuevo usuario
     * @param user
     * @return Usuario
     */
    public User createUser(User user);

    /**
     * Consulta JPA que actualiza un usuario
     * @param user
     * @return Usuario
     */
    public User updateUser(User user);

    /**
     * Consulta JPA que elimina un usuario dado un id
     * @param id
     */
    public void deleteUser(Long id);

    /**
     * Consulta JPA que obtiene un usuario dado un username
     * @param username
     * @return Usuario
     */
    public User loadUserByUsername(String username);

    /**
     * Consulta JPA que obtiene un usuario dado un email
     * @param email
     * @return Usuario
     */
    public User getUserByEmail(String email);
}
