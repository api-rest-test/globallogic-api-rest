package com.globallogic.globallogicapirest.service;

import com.globallogic.globallogicapirest.entity.User;

public interface IUserService {

    /**
     * Consulta JPA que obtiene un usuario dado su username
     * @param username
     * @return Usuario
     */
    public User findByUsername(String username);
}
